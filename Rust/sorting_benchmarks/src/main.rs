use std::io;

fn insertion_sort(arr: & mut [i32; 10]) {
    for i in 1..arr.len() {
        for j in (i+1)..arr.len() {
            if arr[j-1] < arr[i-1] {
                arr.swap(i-1, j-1);
            }
        }
    }
}

fn main(){
    let mut arr: [i32; 10] = [0; 10];

    for x in arr.iter_mut(){
        println!{"Enter a number :: "};
        let mut input_text = String::new();
        io::stdin().read_line(&mut input_text).expect("failed to read");
        match input_text.trim().parse::<i32>() {
            Ok(i) => *x = i,
            Err(_) => println!(":: This is not an integer ::")
        }
    }

    insertion_sort(&mut arr);

    for x in arr.iter(){
        println!("x :: {}", *x);
    }
}
